package com.example.pithitamacbook.ipjsontest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pithitamacbook on 4/25/2017 AD.
 */

public class IPTestResponse {
    @SerializedName("ip") String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
