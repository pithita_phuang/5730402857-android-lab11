package com.example.pithitamacbook.webapicaller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pithitamacbook on 4/25/2017 AD.
 */

public class Result {

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;




    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }



}
