package com.example.pithitamacbook.ipjsontest;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by pithitamacbook on 4/25/2017 AD.
 */

public interface APIService {

    @GET("/")
    Call<IPTestResponse> callJsonTestIp();
}
