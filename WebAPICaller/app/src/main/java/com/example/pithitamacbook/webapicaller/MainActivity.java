package com.example.pithitamacbook.webapicaller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText emailText;
    TextView responseView;
    ProgressBar progressBar;
    String email;
    Button queryButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        responseView = (TextView) findViewById(R.id.responseView);
        emailText = (EditText) findViewById(R.id.emailText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        queryButton = (Button) findViewById(R.id.queryButton);
        queryButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == queryButton){
            email = emailText.getText().toString();
            callService();
        }

        //new RetrieveFeedTask().execute();
    }

    private void callService() {
        Call<Example> call = Manager.getInstance("https://maps.googleapis.com/").CreateService().callLocation("https://maps.googleapis.com/maps/api/geocode/json?address=" + email + "&key=AIzaSyBC8oqXak6uupt2FLCTNDVIRO8WwpiNx30");
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Log.d("lat",response.body().getStatus());
                responseView.setText("Latitude of " + email + " " + response.body().getResults().get(0).getGeometry().getLocation().getLat() +
                "\n" + "Longitude of " + email + " " + response.body().getResults().get(0).getGeometry().getLocation().getLng());
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {

            }
        });
    }

}
