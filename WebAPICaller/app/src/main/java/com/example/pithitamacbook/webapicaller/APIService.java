package com.example.pithitamacbook.webapicaller;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by pithitamacbook on 4/25/2017 AD.
 */

public interface APIService {


    @GET
    Call<Example> callLocation(@Url String url);

//    @GET("maps/api/geocode/")
//    Call<Example> callLocation(,@Query("json?address") String address,@Query("key") String key);
}
