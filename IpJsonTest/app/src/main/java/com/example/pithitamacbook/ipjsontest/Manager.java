package com.example.pithitamacbook.ipjsontest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pithitamacbook on 4/25/2017 AD.
 */

public class Manager {

    private static Manager instance;

    public static Manager getInstance(String url) {
        instance = new Manager(url);
        return instance;
    }

    Retrofit retrofit;

    public Manager(String url) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


        OkHttpClient client = httpClient
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();


        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


    }


    public APIService CreateService() {
        return retrofit.create(APIService.class);
    }
}
